import model
import dao
import helperfunction



def getUser(request,userId):
    if request.method=='GET':

        user=model.userObject()
        user.setUserId(userId)

        queryResult=dao.getUser(user)
        user.transfer_db_to_object(queryResult)

        returnList=model.returnListObject()
        returnList.getUserReturnList(user)


        return model.respondObject().generalRespond(status="OK",message="get user ok",infoList=returnList.getReturnList())


def getUserAllPosts(request,userId):
    if request.method=="GET":


        user=model.userObject()
        user.setUserId(userId)

        queryResult=dao.getAllPosts(user)
        user.transfer_db_to_object(queryResult)

        returnList=model.returnListObject()
        returnList.getUserAllPostsReturnList(user)


        return model.respondObject().generalRespond(status="OK",message="get user ok",infoList=returnList.getReturnList())


def getUserAllComments(request,userId):
    if request.method=="GET":

        user=model.userObject()
        user.setUserId(userId)

        queryResult=dao.getAllComments(user)
        user.transfer_db_to_object(queryResult)

        returnList=model.returnListObject()
        returnList.getUserAllCommentsReturnList(user)


        return model.respondObject().generalRespond(status="OK",message="get user ok",infoList=returnList.getReturnList())




def userAddComment(request,commentId,userId):
    if request.method=="POST":

        user=model.userObject()
        user.setUserId(userId)

        queryResult=dao.getAllComments(user)
        user.transfer_db_to_object(queryResult)

        commentsList=user.getAllComments()
        commentList.append(commentId)

        dao.userAddComment(userId,commentList)

        returnList=model.returnListObject()
        returnList.userAddCommentReturnList(user)

        return model.respondObject().generalRespond(status="OK",message="get user ok",infoList=returnList.getReturnList())



def userAddItem(request,itemId,userId):
    if request.method=="POST":

        user=model.userObject()
        user.setUserId(userId)

        queryResult=dao.getAllPosts(user)
        user.transfer_db_to_object(queryResult)

        commentsList=user.getAllPosts()
        commentList.append(itemId)

        dao.userAddPost(userId,commentList)

        returnList=model.returnListObject()
        returnList.userAddPostReturnList(user)

        return model.respondObject().generalRespond(status="OK",message="get user ok",infoList=returnList.getReturnList())
