import time

import threading


import pymongo

import string
import random
import time

import redis

mongoreadhost="localhost"
mongowritehost="localhost"
mongodport=27017




memsession="localhost"
memverihost="localhost"
memport=11211

writeclient= pymongo.MongoClient(mongowritehost,mongodport,maxPoolSize = 200)
readclient= pymongo.MongoClient(mongoreadhost,mongodport,maxPoolSize = 200)

def db_userAddItem(infoList):

    db=writeclient.newproject
    collection=db.allUsers

    userId=infoList["UserId"]
    allItems=infoList["ItemList"]


    mongodbid=collection.update({"UserId": userId}, {"$set":{"AllItems":allItems}})
    return mongodbid



def db_userAddComment(infoList):


    db=writeclient.newproject
    collection=db.allUsers

    userId=infoList["UserId"]
    allComments=infoList["CommentList"]


    mongodbid=collection.update({"UserId": userId}, {"$set":{"AllComments":allComments}})
    return mongodbid


redis_Host="localhost"
redis_db=1

prodcons_queue="userQueue"

class Task(object):
    def __init__(self):
        self.rcon=redis.StrictRedis(host=redis_Host,db=redis_db)
        self.queue = prodcons_queue

    def listen_task(self):
        while True:

            task = self.rcon.blpop(self.queue, 0)[1]
            k=eval(task)
            if k["daoType"]=="userAddItem":
                db_userAddItem(k)

            elif k["daoType"]=="userAddComment":
                db_userAddComment(k)




if __name__ == '__main__':
    #print '监听任务队列'
    Task().listen_task()
