#database

import redis
# redis_addItem_Host="localhost"
# redis_addItem_db=1
# rcon_addItem=redis.StrictRedis(host=redis_addItem_Host,db=redis_addItem_db)
# prodcons_queue_addItem="addItem"


# redis_addComment_Host="localhost"
# redis_addComment_db=2
# rcon_addComment=redis.StrictRedis(host=redis_addComment_Host,db=redis_addComment_db)
# prodcons_queue_addComment="addComment"


# redis_deleteItem_Host="localhost"
# redis_deleteItem_db=3
# rcon_deleteItem=redis.StrictRedis(host=redis_deleteItem_Host,db=redis_deleteItem_db)
# prodcons_queue_deleteItem="deleteItem"


# redis_deleteComment_Host="localhost"
# redis_deleteComment_db=4
# rcon_deleteComment=redis.StrictRedis(host=redis_deleteComment_Host,db=redis_deleteComment_db)
# prodcons_queue_deleteComment="deleteComment"


redis_Host="localhost"
redis_db=1
rcon=redis.StrictRedis(host=redis_Host,db=redis_db)
prodcons_queue="userQueue"


import pymongo
mongoreadhost="localhost"
mongowritehost="localhost"
mongodport=27017

writeclient= pymongo.MongoClient(mongowritehost,mongodport,maxPoolSize = 200)
readclient= pymongo.MongoClient(mongoreadhost,mongodport,maxPoolSize = 200)


from pymemcache.client.base import Client
memcache_Host="localhost"
memcache_port=11211



def getUser(userObject):
    db=readclient.newproject
    collection=db.allUsers

    userId=userObject.getUserId()

    user=collection.find_one({"UserId":userId})
    return user

def getAllPosts(userObject):
    db=readclient.newproject
    collection=db.allUsers

    userId=userObject.getUserId()

    user=collection.find_one({"UserId":userId})
    return user


def getAllComments(userObject):
    db=readclient.newproject
    collection=db.allUsers

    userId=userObject.getUserId()

    user=collection.find_one({"UserId":userId})
    return user






def userAddComment(userId,commentList):
    daoType="userAddComment"
    infoList={"daoType":daoType,"UserId":userId,"CommentList":commentList}

    rcon.rpush(prodcons_queue,infoList)
    return


def userAddItem(userId,itemList):
    daoType="userAddItem"
    infoList={"daoType":daoType,"UserId":userId,"ItemList":ItemList}

    rcon.rpush(prodcons_queue,infoList)
    return

