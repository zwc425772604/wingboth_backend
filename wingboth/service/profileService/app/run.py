import controller
from flask import request
from flask import Flask

import helpfunction
import model


app = Flask(__name__)




import controller



#return false or userId
def auth_filter(tem_request):
    auth_result=helpfunction.firebase_auth(tem_request)

    return auth_result



@app.route('/u', methods=['GET'])
def getUser():
    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.getUser(request,auth_result)




@app.route('/u/c', methods=['GET'])
def getUserAllComments():
    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.getUserAllComments(request,auth_result)

@app.route('/u/post', methods=['GET'])
def getUserAllPosts():
    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.getUserAllPosts(request,auth_result)




@app.route('/u/c/<commentId>',methods="POST")
def userAddComment(request,commentId):

    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.userAddComment(request,commentId,auth_result)


@app.route('/u/post/<itemId>',methods="POST")
def userAddItem(request,itemId):

    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.userAddItem(request,itemId,auth_result)



if __name__ == '__main__':
    helpfunction.firebase_init()
    app.run(debug=True)
