import time
import helpfunction
import json



class userObject:
    def __init__(self):
        self.userId=""
        self.radius=0
        self.allPosts=[]
        self.allComments=[]

    def transfer_db_to_object(self,queryResult):
        return

    def getUserId(self):
        return self.userId

    def setUserId(self,userId):
        self.userId=userId



    def getRadius(self):
        return self.radius

    def setRadius(self):
        self.radius=radius


    def getAllPosts(self):
        return self.allPosts

    def setAllPosts(self,allPosts):
        self.allPosts=allPosts

    def getAllComments(self):
        return self.allComments

    def setAllComments(self,allComments):
        self.allComments=allComments







class respondObject:
    def __init__(self):
        self.status=""
        self.message=""
        self.returnList={}

    def generalRespond(self,status="",message="",infoList=""):
        self.status=status
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message,"Info":infoList}
        return json.dumps(self.returnList)


    def failedRespond(self,message=""):
        self.status="Error"
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message}
        return json.dumps(self.returnList)

    def okRespond(self,message=""):
        self.status="OK"
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message}
        return json.dumps(self.returnList)




class returnListObject:

    def __init__(self):
        self.returnList={}

    def getUserReturnList(self,userObject):


        userId=userObject.getUserId()

        radius=userObject.getRadius()
        allPosts=userObject.getAllPosts()
        allComments=userObject.getAllComments()

        self.returnList={"UserId":userId,"Radius":radius,"AllPosts":allPosts,"AllComments":allComments}



    def getUserAllCommentsReturnList(self,userObject):
        userId=userObject.getUserId()
        allComments=userObject.getAllComments()

        self.returnList={"UserId":userId,"AllComments":allComments}


    def getUserAllPostsReturnList(self,userObject):

        userId=userObject.getUserId()
        allPosts=userObject.getAllPosts()

        self.returnList={"UserId":userId,"AllPosts":allPosts}


    def userAddCommentReturnList(self,userObject):
        self.returnList={"UserId":userId}


    def userAddItemReturnList(self,userObject):
        self.returnList={"UserId":userId}



    def getReturnList(self):
        return self.returnList
