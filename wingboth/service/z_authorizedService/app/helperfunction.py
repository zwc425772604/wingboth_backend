import string
import random
import time
from passlib.hash import sha256_crypt

salt = 'T7X0NTMJ4VDW01WI'
def id_generator(size=24,chars=string.ascii_uppercase+string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

