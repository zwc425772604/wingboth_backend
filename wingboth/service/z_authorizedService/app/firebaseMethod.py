import model
import dao
import json



import firebase_admin
from firebase_admin import credentials




def init(request):
    if request.method=="POST":
        cred = credentials.Certificate("/root/wingboth-firebase-adminsdk-stftr-6d6cbfb1f3.json")
        firebase_admin.initialize_app(cred)
        return "ok"

def refresh():
    cred = credentials.Certificate("/root/wingboth-firebase-adminsdk-stftr-6d6cbfb1f3.json")
    firebase_admin.initialize_app(cred)


#manage users

from firebase_admin import auth
def retrieveUser(uid):
    user=auth.get_user(uid)
    print user.uid


def getUserByEmail(email):
    user=auth.get_user_by_email(email)
    print user.uid

def createUser():
    user = auth.create_user(
        email='user@example.com',
        email_verified=False,
        phone_number='+15555550100',
        password='secretPassword',
        display_name='John Doe',
        photo_url='http://www.example.com/12345678/photo.png',
        disabled=False)

def updateUser(uid):
    user = auth.update_user(
        uid,
        email='user@example.com',
        phone_number='+15555550100',
        email_verified=True,
        password='newPassword',
        display_name='John Doe',
        photo_url='http://www.example.com/12345678/photo.png',
        disabled=True)

def deleteUser(uid):
    auth.delete_user(uid)

def listAllUser():
    # Start listing users from the beginning, 1000 at a time.
    page = auth.list_users()
    while page:
        for user in page.users:
            print('User: ' + user.uid)
        # Get next batch of users.
        page = page.get_next_page()

    # Iterate through all users. This will still retrieve users in batches,
    # buffering no more than 1000 users in memory at a time.
    for user in auth.list_users().iterate_all():
        print('User: ' + user.uid)




#create custom tokens

def createCustomTokens(uid):
    custom_token=auth.create_custom_token(uid)

def createCustomTokensWithAdditionClaim(uid):
    additional_claims={'location':1.1}
    custom_token=auth.create_custom_token(uid,additional_claims)



def verfiyToken(id_token):
    # id_token comes from the client app (shown above)

    decoded_token = auth.verify_id_token(id_token)
    uid = decoded_token['uid']


def validateCustomUserClaims(uid):
    # Set admin privilege on the user corresponding to uid.
    auth.set_custom_user_claims(uid, {'admin': True})
    # The new custom claims will propagate to the user's ID token the
    # next time a new one is issued.



def verifyIDToken(id_token):
    # Verify the ID token first.
    claims = auth.verify_id_token(id_token)
    if claims['admin'] is True:
        # Allow access to requested admin resource.
        pass

def checkAllClaim(uid):
    user=auth.get_user(uid)
    print (user.custom_claims.get('admin'))










