


app = Flask(__name__, instance_relative_config=True)



import controller
from flask import request
from flask import Flask



@app.route('/init',methods=['POST'])
def init():
    return controller.init(request)

@app.route('/auth', methods=['POST'])
def auth():
    return controller.auth(request)




if __name__ == '__main__':
    app.run(debug=True)
