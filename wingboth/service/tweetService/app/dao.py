#database

import redis
# redis_addItem_Host="localhost"
# redis_addItem_db=1
# rcon_addItem=redis.StrictRedis(host=redis_addItem_Host,db=redis_addItem_db)
# prodcons_queue_addItem="addItem"


# redis_addComment_Host="localhost"
# redis_addComment_db=2
# rcon_addComment=redis.StrictRedis(host=redis_addComment_Host,db=redis_addComment_db)
# prodcons_queue_addComment="addComment"


# redis_deleteItem_Host="localhost"
# redis_deleteItem_db=3
# rcon_deleteItem=redis.StrictRedis(host=redis_deleteItem_Host,db=redis_deleteItem_db)
# prodcons_queue_deleteItem="deleteItem"


# redis_deleteComment_Host="localhost"
# redis_deleteComment_db=4
# rcon_deleteComment=redis.StrictRedis(host=redis_deleteComment_Host,db=redis_deleteComment_db)
# prodcons_queue_deleteComment="deleteComment"


redis_Host="localhost"
redis_db=1
rcon=redis.StrictRedis(host=redis_Host,db=redis_db)
prodcons_queue="projectQueue"


import pymongo
mongoreadhost="localhost"
mongowritehost="localhost"
mongodport=27017

writeclient= pymongo.MongoClient(mongowritehost,mongodport,maxPoolSize = 200)
readclient= pymongo.MongoClient(mongoreadhost,mongodport,maxPoolSize = 200)


from pymemcache.client.base import Client
memcache_Host="localhost"
memcache_port=11211

def dao_addItem(itemObject):
    infoList=itemObject.getListOfObject()
    infoList["daoType"]="addItem"
    rcon.rpush(prodcons_queue,infoList)
    return

def dao_getItem(itemObject):
    db=readclient.newproject
    collection=db.allItems
    itemId=itemObject.getItemId()
    item=collection.find_one({"ItemId":itemId})
    return item


#not yet
# def dao_getRandomItem(userSessionInfoObject):
#     db=readclient.newproject
#     collection=db.allItems


#     userLatitude=userSessionInfoObject.getUserLatitude()
#     userLongtitude=userSessionInfoObject.getUserLongtitude()
#     userSearchLatitude=userSessionInfoObject.getUserSearchLatitude()
#     userSearchLongtitude=userSessionInfoObject.getUserSearchLongtitude()
#     userRadius=userSessionInfoObject.getUserRadius()

    #random search.......................................

    #item=collection.find_one





def dao_addComment(commentObject):
    infoList=commentObject.getListOfObject()
    infoList["daoType"]="addComment"
    rcon.rpush(prodcons_queue,infoList)
    return


def dao_getComment(commentObject):
    db=readclient.newproject
    collection=db.allComments
    commentId=commentObject.getCommentId()
    comment=collection.find_one({"CommentId":commentId})
    return comment

# def dao_deleteComment(commentObject):
#     infoList["daoType"]="deleteComment"
#     infoList["CommentId"]=commentObject.getCommentId()
#     rcon.rpush(prodcons_queue,infoList)
#     return



def dao_getAllCommentsByItemId(itemObject):
    db=readclient.newproject
    collection=db.allComments

    itemId=itemObject.getItemId()

    result=collection.find({"ItemId":itemId})

    return result


#####not yet...................




