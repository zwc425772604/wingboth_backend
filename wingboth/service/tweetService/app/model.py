
import time

import helpfunction
import json
class itemObject:
    def __init__(self):

        self.itemId=helpfunction.id_generator()
        self.title=""
        self.content=""
        self.longitude=0.0
        self.latitude=0.0
        self.timeStamp=int(time.time())
        self.userId=""


    def transfer_json_to_object(self,json_data,userId):

        if "Title" in json_data:
            self.title=json_data.get("Title")
        if "Content" in json_data:
            self.content=json_data.get("Content")
        if "Longitude" in json_data:
            self.longitude=json_data.get("Longitude")
        if "Latitude" in json_data:
            self.latitude=json_data.get("Latitude")

        if "TimeStamp" in json_data:
            self.timeStamp=json_data.get("TimeStamp")

        self.userId=userId


    def transfer_db_to_object(self,queryResult):
        self.itemId=queryResult["ItemId"]
        self.title=queryResult["Title"]
        self.content=queryResult["Content"]
        self.longitude=queryResult["Longitude"]
        self.latitude=queryResult["Latitude"]
        self.timeStamp=queryResult["TimeStamp"]
        self.userId=queryResult["UserId"]

    def getListOfObject(self):
        infoList={"ItemId":self.itemId,"Title":self.title,"Content":self.content,"Longitude":self.longitude,"Latitude":self.latitude,"TimeStamp":self.timeStamp,"UserId":self.userId}
        return infoList

    def getItemId(self):
        return self.itemId

    def setItemId(self,itemId):
        self.itemId=itemId


    def getTitle(self):
        return self.title

    def setTitle(self,title):
        self.title=title


    def getContent(self):
        return self.content

    def setContent(self,content):
        self.content=content



    def getLatitude(self):
        return self.latitude

    def setLatitude(self,latitude):
        self.latitude=latitude


    def getLongitude(self):
        return self.longitude

    def setLongitude(self,longitude):
        self.longitude=longitude


    def getTimeStamp(self):
        return self.timeStamp

    def setTimeStamp(self,timeStamp):
        self.timeStamp=timeStamp

    def getUserId(self):
        return self.userId

    def setUserId(self,userId):
        self.userId=userId



class commentObject:
    def __init__(self):
        self.commentId=helpfunction.id_generator()
        self.commentContent=""
        self.commentLatitude=0.0
        self.commentLongtitude=0.0

        self.timeStamp=int(time.time())
        self.itemId=""
        self.userId=""




    def transfer_json_to_object(self,json_data,itemId,userId):

        if "Content" in json_data:
            self.commentContent=json_data.get("Content")

        if "Longitude" in json_data:
            self.commentLongitude=json_data.get("Longitude")
        if "Latitude" in json_data:
            self.commentLatitude=json_data.get("Latitude")


        self.itemId=itemId
        self.userId=userId

    def transfer_db_to_object(self,queryResult):
        self.itemId=queryResult["ItemId"]
        self.commentContent=queryResult["CommentContent"]
        self.commentId=queryResult["CommentId"]
        self.commentLongitude=queryResult["CommentLongitude"]
        self.commentLatitude=queryResult["CommentLatitude"]
        self.timeStamp=queryResult["TimeStamp"]
        self.userId=queryResult["UserId"]
        self.itemId=queryResult["ItemId"]
        return

    def getListOfObject(self):
        infoList={"CommentId":self.commentId,"CommentContent":self.commentContent,"CommentLatitude":self.commentLatitude,"CommentLongitude":self.commentLongitude,"TimeStamp":self.timeStamp,"ItemId":self.itemId,"UserId":self.userId}

        return infoList

    def getCommentId(self):
        return self.commentId

    def setCommentId(self,commentId):
        self.commentId=commentId



    def getCommentContent(self):
        return self.commentContent

    def setCommentContent(self,content):
        self.commentContent=content
        return


    def getCommentLatitude(self):
        return self.commentLatitude

    def setCommentLatitude(self,latitude):
        self.commentLatitude=latitude
        return

    def getCommentLongitude(self):
        return self.commentLongitude

    def setCommentLongitude(self,longitude):
        self.commentLongitude=longitude
        return


    def getTimeStamp(self):
        return self.timeStamp

    def setTimeStamp(self,timeStamp):
        self.timeStamp=timeStamp
        return

    def getItemId(self):
        return self.itemId

    def setItemId(self,itemId):
        self.itemId=itemId
        return

    def getUserId(self):
        return self.userId
    def setUserId(self,userId):
        self.userId=userId



# class commentsListObject:
#     def __init__(self):
#         self.returnList

class returnListObject:

    def __init__(self):
        self.returnList={}

    def itemGetReturnList(self,itemObject):
        itemId=itemObject.getItemId()
        title=itemObject.getTitle()
        content=itemObject.getContent()
        longitude=itemObject.getLongitude()
        latitude=itemObject.getLatitude()

        timeStamp=itemObject.getTimeStamp()
        userId=itemObject.getUserId()
        self.returnList={"ItemId":itemId,"Title":title,"Content":content,"Longitude":longitude,"Latitude":latitude,"TimeStamp":timeStamp,"UserId":userId}


    def itemRandomGetReturnList(self,itemObject):
        itemId=itemObject.getItemId()
        title=itemObject.getTitle()
        content=itemObject.getContent()
        longitude=itemObject.getLongitude()
        latitude=itemObject.getLatitude()

        timeStamp=itemObject.getTimeStamp()
        userId=itemObject.getUserId()
        self.returnList={"ItemId":itemId,"Title":title,"Content":content,"Longitude":longitude,"Latitude":latitude,"TimeStamp":timeStamp,"UserId":userId}




    def itemAddReturnList(self,itemObject):
        self.returnList={"ItemId":itemObject.getItemId()}


    def commentGetReturnList(self,commentObject):

        commentId=commentObject.getCommentId()
        commentAt=commentObject.getCommentAt()
        commentContent=commentObject.getCommentContent()
        commentLongitude=commentObject.getCommentLongitude()
        commentLatitude=commentObject.getCommentLatitude()


        timeStamp=commentObject.getTimeStamp()
        userId=commentObject.getUserId()
        itemId=commentObject.getItemId()
        self.returnList={"CommentId":commentId,"CommentAt":commentAt,"CommentContent":commentContent,"CommentLongitude":commentLongitude,"CommentLatitude":commentLatitude,"TimeStamp":timeStamp,"UserId":userId,"ItemId":itemId}

        return

    def commentAddReturnList(self,commentObject):
        self.returnList={"CommentId":commentObject.getCommentId()}

    def commentGetAllReturnList(self,queryCursor):
        storeList=[]
        for each in queryCursor:
            eachDic={
            "CommentId":each["CommentId"],
            "CommentContent":each["CommentContent"],
            "CommentLatitude":each["CommentLatitude"],
            "CommentLongtitude":each["CommentLongitude"],
            "TimeStamp":each["TimeStamp"],
            "ItemId":each["ItemId"],
            "UserId":each["UserId"],

            }
            storeList.append(eachDic)
        self.returnList={"AllComments":storeList}
        return

    def getReturnList(self):
        return self.returnList


class respondObject:
    def __init__(self):
        self.status=""
        self.message=""
        self.returnList={}

    def generalRespond(self,status="",message="",infoList=""):
        self.status=status
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message,"Info":infoList}
        return json.dumps(self.returnList)


    def failedRespond(self,message=""):
        self.status="Error"
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message}
        return json.dumps(self.returnList)

    def okRespond(self,message=""):
        self.status="OK"
        self.message=message
        self.returnList={"Status":self.status,"Message":self.message}
        return json.dumps(self.returnList)




