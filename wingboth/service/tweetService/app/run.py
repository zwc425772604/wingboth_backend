

import controller

from flask import request
from flask import Flask
import helpfunction

import model



app = Flask(__name__)



def auth_filter(tem_request):
    auth_result=helpfunction.firebase_auth(tem_request)

    return auth_result










@app.route('/item',methods=["POST"])
def itemAdd():

    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.itemAdd(request,auth_result)




@app.route('/item/<itemId>',methods=["GET"])
def itemGet(itemId):
    return controller.itemGet(request,itemId)




# @app.route('/itemRandomGet',methods=["GET"])
# def itemRandomGet(itemId):
#     return controller.itemRandomGet(request)







@app.route('/item/<itemId>/c',methods=["POST"])
def commentAdd(itemId):
    auth_result=auth_filter(request)
    if auth_result=="False":
        return model.respondObject().generalRespond(status="Error",message="have to login")

    else:
        return controller.commentAdd(request,itemId,auth_result)






@app.route('/item/<itemId>/c',methods=["GET"])
def commentGetAll(itemId):
    return controller.commentGetAll(request,itemId)



@app.route('/c/<commentId>',methods=["GET"])
def commentGet(commentId):
    return controller.commentGet(request,commentId)




if __name__ == '__main__':
    helpfunction.firebase_init()
    app.run(debug=True)