#from flask import Flask
import model
import dao
import json


import helpfunction


def itemAdd(request,userId):
    if request.method =='POST':
        #try:

        data=request.get_data()
        json_data=json.loads(data.decode('utf-8'))

        if json_data is None:
            return model.respondObject().generalRespond(status="Error",message="no content")


        item=model.itemObject()



        item.transfer_json_to_object(json_data,userId)

        dao.dao_addItem(item)  #to redis queue
        returnList=model.returnListObject()
        returnList.itemAddReturnList(item)

        return model.respondObject().generalRespond(status="OK",message="add item ok",infoList=returnList.getReturnList())
        #except:

        #    return model.respondObject().failedRespond("exception")







def itemGet(request,itemId):
    if request.method=="GET":

        item=model.itemObject()
        item.setItemId(itemId)

        #queryResult=dao.dao_getItemByItemId(itemId)
        queryResult=dao.dao_getItem(item)
        item.transfer_db_to_object(queryResult)


        returnList=model.returnListObject()
        returnList.itemGetReturnList(item)
        return model.respondObject().generalRespond(status="OK",message="get item ok",infoList=returnList.getReturnList())





















#not yet
# def itemRandomGet(request):
#     if request.method=="GET":
#         item=model.itemObject()

#         user=model.userSessionInfoObject()
#         user.transfer_session_to_object()



#         queryResult=dao.dao_getRandomItem(user)


#         item.transfer_db_to_object(queryResult)


#         returnList=model.returnListObject()
#         returnList.itemRandomGetReturnList(item)
#         return model.respondObject().generalRespond(status="OK",message="get item ok",infoList=returnList.getReturnList())




def commentAdd(request,itemId,userId):
    if request.method=="POST":

        data=request.get_data()
        json_data=json.loads(data.decode('utf-8'))

        if json_data is None:
            return model.respondObject().generalRespond(status="Error",message="no content")



        comment=model.commentObject()


        comment.transfer_json_to_object(json_data,itemId,userId)


        dao.dao_addComment(comment)
        returnList=model.returnListObject()
        returnList.commentAddReturnList(comment)
        return model.respondObject().generalRespond(status="OK",message="add comment ok",infoList=returnList.getReturnList())


def commentGet(request,commentId):#put it in redis first, then get from redis
    if request.method=="GET":

        comment=model.commentObject()
        comment.setCommentId(commentId)

        queryResult=dao.dao_getComment(comment)

        comment.transfer_db_to_object(queryResult)

        returnList=model.returnListObject()
        returnList.commentGetReturnList(comment)
        return model.respondObject().generalRespond(status="OK",message="get comment ok",infoList=returnList.getReturnList())











def commentGetAll(request,itemId):
    if request.method=="GET":
        #searchService.........

        item=model.itemObject()
        item.setItemId(itemId)
        queryResult=dao.dao_getAllCommentsByItemId(item)

        # allComment=model.commentListObject()
        # allComment.transfer_db_to_object(queryResult)

        returnList=model.returnListObject()
        returnList.commentGetAllReturnList(queryResult)
        return model.respondObject().generalRespond(status="OK",message="get all comment ok",infoList=returnList.getReturnList())









