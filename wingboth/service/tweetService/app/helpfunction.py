
import string
import random
import time



import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth





firebase_json="/root/wingboth-firebase-adminsdk-stftr-6d6cbfb1f3.json"

def firebase_init():
    cred=credentials.Certificate(firebase_json)
    firebase_admin.initialize_app(cred)



def firebase_auth(request):

    try:
        id_token=request.headers["Authorization"]
        decoded_token=auth.verify_id_token(id_token)
        uid=decoded_token['uid']
    except:
        return "False"
    else:
        return uid








def id_generator(size=24,chars=string.ascii_uppercase+string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

