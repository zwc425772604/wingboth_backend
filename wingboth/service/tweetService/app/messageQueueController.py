import time

import threading


import pymongo

import string
import random
import time

import redis

mongoreadhost="localhost"
mongowritehost="localhost"
mongodport=27017




memsession="localhost"
memverihost="localhost"
memport=11211

writeclient= pymongo.MongoClient(mongowritehost,mongodport,maxPoolSize = 200)
readclient= pymongo.MongoClient(mongoreadhost,mongodport,maxPoolSize = 200)

def db_addItem(infoList):

    db=writeclient.newproject
    collection=db.allItems

    itemId=infoList["ItemId"]
    title=infoList["Title"]
    content=infoList["Content"]
    longitude=infoList["Longitude"]
    latitude=infoList["Latitude"]
    timeStamp=infoList["TimeStamp"]
    userId=infoList["UserId"]

    mongodbid=collection.insert({"ItemId": itemId, "UserId":userId,"Title":title,"Content":content,"Longitude":longitude,"Latitude":latitude,'TimeStamp':timeStamp})
    return mongodbid

def db_deleteItem(infoList):
    db=writeclient.newproject
    collection=db.allItems
    itemId=infoList["ItemId"]
    item=collection.remove({"Itemid":itemId})
    return

def db_addComment(infoList):


    db=writeclient.newproject
    collection=db.allComments

    commentId=infoList["CommentId"]
    commentContent=infoList["CommentContent"]
    commentLongitude=infoList["CommentLongitude"]
    commentLatitude=infoList["CommentLatitude"]


    userId=infoList["UserId"]
    itemId=infoList["ItemId"]


    mongodbid=collection.insert({"ItemId": itemId, "UserId":userId,"CommentId":commentId,"CommentContent":commentContent,"CommentLongitude":commentLongitude,"CommentLatitude":commentLatitude,'TimeStamp':timeStamp})
    return mongodbid

def db_deleteComment(infoList):
    db=writeclient.newproject
    collection=db.allComments
    commentId=infoList["CommentId"]
    comment=collection.remove({"CommentId":commentId})
    return



redis_Host="localhost"
redis_db=1

prodcons_queue="projectQueue"

class Task(object):
    def __init__(self):
        self.rcon=redis.StrictRedis(host=redis_Host,db=redis_db)
        self.queue = prodcons_queue

    def listen_task(self):
        while True:

            task = self.rcon.blpop(self.queue, 0)[1]
            k=eval(task)
            if k["daoType"]=="addItem":
                db_addItem(k)
            elif k["daoType"]=="deleteItem":
                db_deleteItem(k)
            elif k["daoType"]=="addComment":
                db_addComment(k)
            elif k["daoType"]=="deleteComment":
                db_deleteComment(k)



if __name__ == '__main__':
    #print '监听任务队列'
    Task().listen_task()
