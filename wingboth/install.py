import os


def install_function():
    #install nginx
    os.system('sudo apt-get update')
    os.system('sudo apt-get install nginx')

    #install docker
    os.system('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
    os.system('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
    os.system('sudo apt-get update')
    os.system('apt-cache policy docker-ce')
    os.system('sudo apt-get install -y docker-ce')
    os.system('sudo apt-get update')

    #install docker-compose
    os.system('sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose')
    os.system('sudo chmod +x /usr/local/bin/docker-compose')

    #install postgresql
    os.system('sudo apt-get install postgresql postgresql-contrib')

    #install virtualenv
    os.system('sudo apt-get install python3-pip python3-dev')
    os.system('sudo pip3 install virtualenv')
    os.system('sudo mkdir startup')
    os.system('virtualenv startup/newproject')

    #install memcached
    os.system('docker run --name memcached -d -p 11211:11211 romeoz/docker-memcached')

    #install mongodb
    os.system('docker run --name mongod -p 27017:27017 -d mongo')
    #sudo docker exec -it mongod /bin/bash


    #install postfix for email #need configuration to send to Gmail
    os.system('sudo apt-get install postfix')
    os.system('sudo apt-get install mailutils')

    #install redis
    os.system('docker run --name redis -d redis redis-server --requirepass startup')
    #os.system('docker run -it --link redis:redis --rm redis redis-cli -h redis -p 6379') #redis commandline

    #install passlib for password hashing
    os.system('pip3 install passlib')

    #install redis server
    os.system('sudo apt-get install redis-server')
    #clone the project
    #os.system('git clone https://zwc425772604@bitbucket.org/RunnanP/startup.git')


install_function()
